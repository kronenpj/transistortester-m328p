#!/usr/bin/env bash

PROJ="TransistorTester-m328p"
zip -9m ${PROJ}-gerber.zip *gbr* *drl
