# UNTESTED - Under development

This is an adaptation of [TransistorTester](https://www.mikrocontroller.net/articles/AVR_Transistortester) to use the Arduino Pro Mini. Inspired and heavily influenced by [Stefan Wagner's](https://easyeda.com/wagiminator/y-atmega-transistortester-smd) implementation. This is the reason the resistor numbering starts at R3, for instance.

This version utilizes a "raw" ATMega328P with internal clock. I have yet to verify that this project works without a crystal. 

## Bill of materials:
* 1x U1 - ATMega328P DIP (w/optional 28-pin narrow DIP socket)
* 1x U2 - 2.5v Precision Voltage Reference, LM4040DBZ-2.5, SMD SOT-23-3 footprint, Optional
* 1x U3 - I2C 0.96" OLED Display
* 1x J1 - 6-pin Female, 2.54mm Header Socket
* 1x J2 - USB Micro-B, Molex_47346-0001 (Used for power only)
* 1x J3 - 6-pin Male, 2x3, 2.54mm Header, Optional (ICSP interface)
* 1x SW1 - Push Button Switch 6mm (SW1), or 12mm (SW2)
* 1x R3 - 27k Ohm resistor, SMD 0603 (1608 metric) footprint
* 1x R10 - 2.2k Ohm resistor, SMD 0603 (1608 metric) footprint
* 3x R4, R6, R8 - 680 Ohm resistor, SMD 0603 (1608 metric) footprint, use 1% or better precision
* 3x R5, R7, R9 - 470k Ohm resistor, SMD 0603 (1608 metric) footprint, use 1% or better precision
* 2x C1, C2 - 100nF capacitor, SMD 0603 (1608 metric)
* 1x C3 - 10nF capacitor, SMD 0603 (1608 metric)

## References and Sources
* [Article](https://www.mikrocontroller.net/articles/AVR_Transistortester)
* Original Code Repository: svn://mikrocontroller.net/transistortester
* [Code Browser](https://www.mikrocontroller.net/svnbrowser/transistortester/)
* [My code repository](https://gitlab.com/kronenpj/transistortester)
* [KiCAD repository](https://gitlab.com/kronenpj/transistortester-promini)
* [Stefan's Design](https://easyeda.com/wagiminator/y-atmega-transistortester-smd)
